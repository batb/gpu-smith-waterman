NVC = nvcc
OPT = -O3 --compiler-options -Wall,-O3,-march=native
ARC = -arch=sm_80

gaaswa: main.cu Makefile
	$(NVC) $(OPT) $(ARC) -o $@ $<
	ls -l $@

gaaswa_int16: main.cu Makefile
	$(NVC) $(OPT) -DUSE_INT16 $(ARC) -o $@ $<
	ls -l $@

test_small: gaaswa
	./gaaswa data/query.fa data/databaseRandom.fa

test_large: gaaswa
	./gaaswa data/query.fa data/databaseRandomLarge.fa

test_small_int16: gaaswa_int16
	./gaaswa_int16 data/query.fa data/databaseRandom.fa

test_large_int16: gaaswa_int16
	./gaaswa_int16 data/query.fa data/databaseRandomLarge.fa

.PHONY: clean
clean:
	rm -f gaaswa gaaswa_int16

