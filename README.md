## GPU All-to-All Smith-Waterman Aligner (GAASWA)

The GPU All-to-All Smith-Waterman Aligner (GAASWA) is an open-source high-performance protein sequence alignment tool optimized for Graphics Processing Units (GPUs). It leverages the parallel processing capabilities of GPUs to accelerate the Smith-Waterman algorithm, making it especially beneficial for aligning query proteins against database proteins.

### Features:
- **GPU Acceleration**: Utilizes the parallel processing power of modern GPUs for rapid sequence alignments.
- **Protein-Specific**: Designed specifically for protein sequences with a hardcoded BLOSUM62 matrix, gap open and extend penalties can be specified.
- **Alignment Scores**: Outputs the alignment scores, highlighting the best matches. Note that it doesn't perform backtracing.
- **Int16 or Int32 modes**: The alignment scores that overflow the Int8 type can be calculated in either Int16 or Int32. (Int16 handles scores up to 32767, less memory bandwidth compared to Int32). Int32 by default but Int16 may perform slightly faster.
- **MultiGPU**: Automatically detects available GPUs and runs the alignment on all GPUs. Make sure that the database is randomized to achieve the highest performance with multiple GPUs.

### Compilation:

You can use the `make` command to compile. Here's a simple example of using `make`:

```bash
make gaaswa
```

**Note**: Adjust the `-arch` flag according to your GPU's compute capability in the `Makefile`. Refer to NVIDIA's documentation for the appropriate flag for your GPU model.


### Usage:

To use the GPU All-to-All Smith-Waterman Aligner, ensure your input files are in FASTA format and follow the command structure below:

```bash
./gaaswa query_sequence_file database_file [-top topScoresToPrint] [-gapo value] [-gape value]
```

#### Example:

Align query protein sequences (`query.fa`) against a protein database (`database.fa`), and specify optional parameters:

```bash
./gaaswa query.fa database.fa -top 5 -gapo 11 -gape 1
```

---

**Note**: Ensure you have the appropriate GPU drivers, CUDA toolkit, and other dependencies installed before compiling and running the aligner.