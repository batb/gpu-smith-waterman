#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <unordered_map>
#include <algorithm>
#include <tuple>
#include <numeric>
#include <chrono>
#include <queue>

#define DBCACHEBLOCK 4 //must be 4
#define QCACHEBLOCK 4
#define WARPSIZE 32
#define BLOCKSIZE 512 //cannot support more than 512 due to shared memory limit
#define INTOVERFLOW 116 //127 - 11 (max BLOSUM62) scores higher than this needs to be re-evaluated

typedef char scoreInt;
typedef char4 packed4Int; //packed datatype
#ifdef USE_INT16
typedef short scoreIntLonger; //just 16bit integer
typedef short4 packed4IntLonger; //packed 4 shorts
#else
typedef int scoreIntLonger; //just 32bit integer
typedef int4 packed4IntLonger; //packed 4 ints
#endif

__constant__ scoreInt qArray_constant[50000]; //50000 is just a large number that fits into constant memory

//padded with 0's for dummy letter
scoreInt s_blosum62[] =
{
 4,-1,-2,-2,0,-1,-1,0,-2,-1,-1,-1,-1,-2,-1,1,0,-3,-2,0,-2,-1,0,-4,0,
 -1,5,0,-2,-3,1,0,-2,0,-3,-2,2,-1,-3,-2,-1,-1,-3,-2,-3,-1,0,-1,-4,0,
 -2,0,6,1,-3,0,0,0,1,-3,-3,0,-2,-3,-2,1,0,-4,-2,-3,3,0,-1,-4,0,
 -2,-2,1,6,-3,0,2,-1,-1,-3,-4,-1,-3,-3,-1,0,-1,-4,-3,-3,4,1,-1,-4,0,
 0,-3,-3,-3,9,-3,-4,-3,-3,-1,-1,-3,-1,-2,-3,-1,-1,-2,-2,-1,-3,-3,-2,-4,0,
-1,1,0,0,-3,5,2,-2,0,-3,-2,1,0,-3,-1,0,-1,-2,-1,-2,0,3,-1,-4,0,
-1,0,0,2,-4,2,5,-2,0,-3,-3,1,-2,-3,-1,0,-1,-3,-2,-2,1,4,-1,-4,0,
0,-2,0,-1,-3,-2,-2,6,-2,-4,-4,-2,-3,-3,-2,0,-2,-2,-3,-3,-1,-2,-1,-4,0,
-2,0,1,-1,-3,0,0,-2,8,-3,-3,-1,-2,-1,-2,-1,-2,-2,2,-3,0,0,-1,-4,0,
-1,-3,-3,-3,-1,-3,-3,-4,-3,4,2,-3,1,0,-3,-2,-1,-3,-1,3,-3,-3,-1,-4,0,
-1,-2,-3,-4,-1,-2,-3,-4,-3,2,4,-2,2,0,-3,-2,-1,-2,-1,1,-4,-3,-1,-4,0,
-1,2,0,-1,-3,1,1,-2,-1,-3,-2,5,-1,-3,-1,0,-1,-3,-2,-2,0,1,-1,-4,0,
-1,-1,-2,-3,-1,0,-2,-3,-2,1,2,-1,5,0,-2,-1,-1,-1,-1,1,-3,-1,-1,-4,0,
-2,-3,-3,-3,-2,-3,-3,-3,-1,0,0,-3,0,6,-4,-2,-2,1,3,-1,-3,-3,-1,-4,0,
-1,-2,-2,-1,-3,-1,-1,-2,-2,-3,-3,-1,-2,-4,7,-1,-1,-4,-3,-2,-2,-1,-2,-4,0,
1,-1,1,0,-1,0,0,0,-1,-2,-2,0,-1,-2,-1,4,1,-3,-2,-2,0,0,0,-4,0,
0,-1,0,-1,-1,-1,-1,-2,-2,-1,-1,-1,-1,-2,-1,1,5,-2,-2,0,-1,-1,0,-4,0,
-3,-3,-4,-4,-2,-2,-3,-2,-2,-3,-2,-3,-1,1,-4,-3,-2,11,2,-3,-4,-3,-2,-4,0,
-2,-2,-2,-3,-2,-1,-2,-3,2,-1,-1,-2,-1,3,-3,-2,-2,2,7,-1,-3,-2,-1,-4,0,
0,-3,-3,-3,-1,-2,-2,-3,-3,3,1,-2,1,-1,-2,-2,0,-3,-1,4,-3,-2,-1,-4,0,
-2,-1,3,4,-3,0,1,-1,0,-3,-4,0,-3,-3,-2,0,-1,-4,-3,-3,4,1,-1,-4,0,
-1,0,0,1,-3,3,4,-2,0,-3,-3,1,-1,-3,-1,0,-1,-3,-2,-2,1,4,-1,-4,0,
0,-1,-1,-1,-2,-1,-1,-1,-1,-1,-1,-1,-1,-1,-2,0,0,-2,-1,-1,-1,-1,-1,-4,0,
-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,1,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
};

void printMemoryUsage() {
    size_t free_byte;
    size_t total_byte;
    cudaError_t cuda_status = cudaMemGetInfo( &free_byte, &total_byte );

    if ( cudaSuccess != cuda_status ){
        std::cout << "Error: " << cudaGetErrorString(cuda_status) << std::endl;
        return;
    }

    double free_db = (double)free_byte;
    double total_db = (double)total_byte;
    double used_db = total_db - free_db;
    std::cout << "GPU memory usage: " << used_db/1024.0/1024.0 << " MB used out of " << total_db/1024.0/1024.0 << " MB" << std::endl;
}

std::tuple<std::vector<scoreInt>, std::vector<size_t>, std::vector<size_t>, size_t, std::vector<size_t>, std::vector<std::string>> fastaReader(std::string fileName, size_t cacheBlock){
    std::ifstream file(fileName);

    char myChar;
    std::vector<std::string> proteinNames;
    std::string proteinName;
    int proteinIndex = -1;
    std::vector<char> sequence;
    size_t currentOffset = 0;
    std::vector<size_t> startOffsets;
    std::vector<size_t> endOffsets;
    std::vector<size_t> letterCounts; //without the padding

    while(file.get(myChar)){
        if(myChar == '>'){ //Start of new protein
            //pad the older protein with dummy letters
            size_t remainder = currentOffset % cacheBlock;
            if(remainder != 0){
                for(size_t i = cacheBlock; i > remainder; i--){
                    sequence.push_back('-');
                    currentOffset++;
                }
            }

            proteinIndex++;
            letterCounts.push_back(0);

            startOffsets.push_back(currentOffset);
            if(proteinIndex != 0) endOffsets.push_back(currentOffset);

            std::getline(file, proteinName); //get name
            proteinNames.push_back(proteinName);
        }
        else{
            if(myChar != '\n'){
            sequence.push_back(myChar); //add character to the sequence
            letterCounts[proteinIndex]++;
            currentOffset++;
            }
        }
    }
    //pad the final protein with dummy letters
    size_t remainder = currentOffset % cacheBlock;
    if(remainder != 0){
        for(size_t i = cacheBlock; i > remainder; i--){
            sequence.push_back('-');
            currentOffset++;
        }
    }

    endOffsets.push_back(currentOffset); //at the end of the file we also note the offset

    //convert sequence to uppercase
    std::transform(sequence.begin(), sequence.end(), sequence.begin(), ::toupper);

    //Converting the amino acids to int
    // Define the amino acid characters
    std::string aminoAcids = "ARNDCQEGHILKMFPSTWYVBZX*-";

    // Create a lookup table that maps each amino acid character to an integer
    std::unordered_map<scoreInt, scoreInt> amino_acid_to_int;
    for (size_t i = 0; i < aminoAcids.size(); i++) {
        amino_acid_to_int[aminoAcids[i]] = static_cast<scoreInt>(i);
    }

    std::vector<scoreInt> intSequence;
    for(auto elem: sequence){
        intSequence.push_back(amino_acid_to_int[elem]);
    }

    return std::make_tuple(intSequence, startOffsets, endOffsets, static_cast<size_t>(proteinIndex + 1), letterCounts, proteinNames);
};

__global__ void alignSingleQueryOverflown(size_t qLength, scoreInt* dbArray, size_t overflowNum, size_t* dbStartOffsets, size_t* dbEndOffsets, packed4IntLonger* Hold, packed4IntLonger* Hnew, packed4IntLonger* Eold, packed4IntLonger* Enew, scoreInt* blosum, scoreIntLonger* scores, scoreIntLonger Q, scoreIntLonger R){

    //put blosum matrix into shared
    __shared__ scoreInt blosum_shared[25 * 25];
    for(size_t t = threadIdx.x; t < 25 * 25; t+=BLOCKSIZE){
        blosum_shared[t] = blosum[t];
    }
    __syncthreads();

    size_t threadId = blockIdx.x * blockDim.x + threadIdx.x; //assuming the number of threads is equal to the db size

    if(threadId < overflowNum){
        //figure out offsets from your threadID
        size_t offsetStart = dbStartOffsets[threadId];
        size_t offsetEnd = dbEndOffsets[threadId];

        //initialize the max score
        scoreIntLonger Smax = 0;

        packed4IntLonger* temp; //used for swapping pointers

#ifdef USE_INT16
        //initialize the Hnew, Enew
        Hnew[0 * overflowNum + threadId] = make_short4(0,0,0,0);
        Enew[0 * overflowNum + threadId] = make_short4(0,0,0,0);
        packed4IntLonger HnewTemp2 = make_short4(0,0,0,0);
        packed4IntLonger EnewTemp = make_short4(0,0,0,0);
#else
        //initialize the Hnew, Enew
        Hnew[0 * overflowNum + threadId] = make_int4(0,0,0,0);
        Enew[0 * overflowNum + threadId] = make_int4(0,0,0,0);
        packed4IntLonger HnewTemp2 = make_int4(0,0,0,0);
        packed4IntLonger EnewTemp = make_int4(0,0,0,0);
#endif

        int blosumRow;

        scoreIntLonger HoldTemp1w;
        scoreIntLonger HoldTemp2w = 0;

        packed4IntLonger HnewTemp1;
        scoreIntLonger EoldTempw;

        scoreInt dbLetters4[4];
        for(size_t i = offsetStart; i < offsetEnd; i+=4){
        //init with 0s
#ifdef USE_INT16
            Hnew[0 * overflowNum + threadId] = make_short4(0,0,0,0);
#else
            Hnew[0 * overflowNum + threadId] = make_int4(0,0,0,0);
#endif

            scoreIntLonger Fx = 0;
            scoreIntLonger Fy = 0;
            scoreIntLonger Fz = 0;
            scoreIntLonger Fw = 0;

            //uncoalesced access to dbArray
            dbLetters4[0] = dbArray[i];
            dbLetters4[1] = dbArray[i+1];
            dbLetters4[2] = dbArray[i+2];
            dbLetters4[3] = dbArray[i+3];

            for(int j = 1; j <= qLength; j++){
                //load the values in coalesced manner (only loads old values)
                EoldTempw = Eold[j * overflowNum + threadId].w; //coalesced with stride 4
                HoldTemp1w = HoldTemp2w;
                HoldTemp2w = Hold[j * overflowNum + threadId].w; //coalesced with stride 4
                HnewTemp1 = HnewTemp2;

                blosumRow = qArray_constant[j - 1] * 25;

                //x
                HnewTemp2.x = HoldTemp1w + blosum_shared[blosumRow + dbLetters4[0]];
                EnewTemp.x = max(EoldTempw - R, HoldTemp2w - Q);
                HnewTemp2.x = max(HnewTemp2.x, EnewTemp.x);
                Fx         = max(Fx - R,         HnewTemp1.x - Q);
                HnewTemp2.x = max(HnewTemp2.x, Fx);
                HnewTemp2.x = max(HnewTemp2.x, 0);
                Smax = max(HnewTemp2.x, Smax);

                //y
                HnewTemp2.y = HnewTemp1.x + blosum_shared[blosumRow + dbLetters4[1]];
                EnewTemp.y = max(EnewTemp.x - R, HnewTemp2.x - Q);
                HnewTemp2.y = max(HnewTemp2.y, EnewTemp.y);
                Fy         = max(Fy - R,         HnewTemp1.y - Q);
                HnewTemp2.y = max(HnewTemp2.y, Fy);
                HnewTemp2.y = max(HnewTemp2.y, 0);
                Smax = max(HnewTemp2.y, Smax);

                //z
                HnewTemp2.z = HnewTemp1.y + blosum_shared[blosumRow + dbLetters4[2]];
                EnewTemp.z = max(EnewTemp.y - R, HnewTemp2.y - Q);
                HnewTemp2.z = max(HnewTemp2.z, EnewTemp.z);
                Fz         = max(Fz - R,         HnewTemp1.z - Q);
                HnewTemp2.z = max(HnewTemp2.z, Fz);
                HnewTemp2.z = max(HnewTemp2.z, 0);
                Smax = max(HnewTemp2.z, Smax);

                //w
                HnewTemp2.w = HnewTemp1.z + blosum_shared[blosumRow + dbLetters4[3]];
                EnewTemp.w = max(EnewTemp.z - R, HnewTemp2.z - Q);
                HnewTemp2.w = max(HnewTemp2.w, EnewTemp.w);
                Fw         = max(Fw - R,         HnewTemp1.w - Q);
                HnewTemp2.w = max(HnewTemp2.w, Fw);
                HnewTemp2.w = max(HnewTemp2.w, 0);
                Smax = max(HnewTemp2.w, Smax);

                //store the values in perfectly coalesced manner
                Enew[j * overflowNum + threadId] = EnewTemp;
                Hnew[(j-1) * overflowNum + threadId] = HnewTemp1;
            }
            Hnew[qLength * overflowNum + threadId] = HnewTemp2;

            //swap old and new scoring matrix columns
            temp = Hold;
            Hold = Hnew;
            Hnew = temp;
            temp = Eold;
            Eold = Enew;
            Enew = temp;
        }
        
        //update the scores
        scores[threadId] = Smax;
    }
};

__global__ void alignSingleQuery(size_t qLength, scoreInt* dbArray, size_t dbSize, size_t* dbStartOffsets, size_t* dbEndOffsets, size_t* warpDBOffsets, scoreInt* blosum, scoreInt* scores, packed4Int* Hrow, packed4Int* Frow, scoreInt Q, scoreInt R){

    size_t threadId = blockIdx.x * blockDim.x + threadIdx.x; //assuming the number of threads is equal to the db size
    size_t warpId = threadId / WARPSIZE;
    size_t threadInWarpId = threadId % WARPSIZE;
    size_t warpDBOffset = warpDBOffsets[warpId];

    //shared implementation of the columns
    __shared__ packed4Int HoldShared[(QCACHEBLOCK+1) * BLOCKSIZE];
    __shared__ packed4Int HnewShared[(QCACHEBLOCK+1) * BLOCKSIZE];
    __shared__ packed4Int EoldShared[(QCACHEBLOCK+1) * BLOCKSIZE];
    __shared__ packed4Int EnewShared[(QCACHEBLOCK+1) * BLOCKSIZE];

    //initialize to 0s
    for(size_t i = 0; i < (QCACHEBLOCK+1); i++){
        HoldShared[i * BLOCKSIZE + threadIdx.x] = make_char4(0,0,0,0);
        EoldShared[i * BLOCKSIZE + threadIdx.x] = make_char4(0,0,0,0);
    }

    //put blosum matrix into shared
    __shared__ scoreInt blosum_shared[25 * 25];
    for(size_t t = threadIdx.x; t < 25 * 25; t+=BLOCKSIZE){
        blosum_shared[t] = blosum[t];
    }

    __syncthreads();

    //these pointers are useful for swapping
    packed4Int* Hold = HoldShared;
    packed4Int* Eold = EoldShared;
    packed4Int* Hnew = HnewShared;
    packed4Int* Enew = EnewShared;

    if(threadId < dbSize){
        //figure out offsets from your threadID
        size_t offsetStart = dbStartOffsets[threadId];
        size_t offsetEnd = dbEndOffsets[threadId];

        //initialize the max score
        scoreInt Smax = 0;

        packed4Int* temp; //used for swapping pointers

        scoreInt dbLetters4[4];

        //initialize the Hnew, F
        Hnew[0 * BLOCKSIZE + threadIdx.x] = make_char4(0, 0, 0, 0);
        scoreInt Fx = 0;
        scoreInt Fy = 0;
        scoreInt Fz = 0;
        scoreInt Fw = 0;

        scoreInt HoldTemp1w;
        scoreInt HoldTemp2w;

        packed4Int HnewTemp1;
        packed4Int HnewTemp2;

        scoreInt EoldTempw;
        packed4Int EnewTemp;

        scoreInt diag;

        int blosumRow;

        //outer loop divides the query protein
        for(size_t jStart = 0; jStart < qLength; jStart += QCACHEBLOCK){
            //init with 0s
            HoldTemp2w = 0;
            //this loop divides the database protein
            for(size_t i = offsetStart; i < offsetEnd; i += DBCACHEBLOCK){
                //init with 0s
                Hnew[0 * BLOCKSIZE + threadIdx.x] = make_char4(0, 0, 0, 0);

                //uncoalesced access to dbArray
                dbLetters4[0] = dbArray[i];
                dbLetters4[1] = dbArray[i+1];
                dbLetters4[2] = dbArray[i+2];
                dbLetters4[3] = dbArray[i+3];
                
                //load horizontal H and F (only 4 values)
                HnewTemp2 = Hrow[warpDBOffset + (i - offsetStart)/DBCACHEBLOCK * WARPSIZE + threadInWarpId];
                Fx = Frow[warpDBOffset + (i - offsetStart)/DBCACHEBLOCK * WARPSIZE + threadInWarpId].x;
                Fy = Frow[warpDBOffset + (i - offsetStart)/DBCACHEBLOCK * WARPSIZE + threadInWarpId].y;
                Fz = Frow[warpDBOffset + (i - offsetStart)/DBCACHEBLOCK * WARPSIZE + threadInWarpId].z;
                Fw = Frow[warpDBOffset + (i - offsetStart)/DBCACHEBLOCK * WARPSIZE + threadInWarpId].w;
                //store diagonal value for later use
                diag = HnewTemp2.w;

                //inner loop over small block that fit into cache
                for(size_t j = 1; j < QCACHEBLOCK + 1; j++){
                    //load the values (only loads old values)
                    EoldTempw = Eold[j * BLOCKSIZE + threadIdx.x].w;
                    HoldTemp1w = HoldTemp2w;
                    HoldTemp2w = Hold[j * BLOCKSIZE + threadIdx.x].w;
                    HnewTemp1 = HnewTemp2;

                    blosumRow = qArray_constant[j + jStart - 1] * 25;

                    //x
                    HnewTemp2.x = HoldTemp1w + blosum_shared[blosumRow + dbLetters4[0]];
                    EnewTemp.x = max(EoldTempw - R, HoldTemp2w - Q);
                    HnewTemp2.x = max(HnewTemp2.x, EnewTemp.x);
                    Fx         = max(Fx - R,         HnewTemp1.x - Q);
                    HnewTemp2.x = max(HnewTemp2.x, Fx);
                    HnewTemp2.x = max(HnewTemp2.x, 0);
                    Smax = max(HnewTemp2.x, Smax);

                    //y
                    HnewTemp2.y = HnewTemp1.x + blosum_shared[blosumRow + dbLetters4[1]];
                    EnewTemp.y = max(EnewTemp.x - R, HnewTemp2.x - Q);
                    HnewTemp2.y = max(HnewTemp2.y, EnewTemp.y);
                    Fy         = max(Fy - R,         HnewTemp1.y - Q);
                    HnewTemp2.y = max(HnewTemp2.y, Fy);
                    HnewTemp2.y = max(HnewTemp2.y, 0);
                    Smax = max(HnewTemp2.y, Smax);

                    //z
                    HnewTemp2.z = HnewTemp1.y + blosum_shared[blosumRow + dbLetters4[2]];
                    EnewTemp.z = max(EnewTemp.y - R, HnewTemp2.y - Q);
                    HnewTemp2.z = max(HnewTemp2.z, EnewTemp.z);
                    Fz         = max(Fz - R,         HnewTemp1.z - Q);
                    HnewTemp2.z = max(HnewTemp2.z, Fz);
                    HnewTemp2.z = max(HnewTemp2.z, 0 );
                    Smax = max(HnewTemp2.z, Smax);

                    //w
                    HnewTemp2.w = HnewTemp1.z + blosum_shared[blosumRow + dbLetters4[3]];
                    EnewTemp.w = max(EnewTemp.z - R, HnewTemp2.z - Q);
                    HnewTemp2.w = max(HnewTemp2.w, EnewTemp.w);
                    Fw         = max(Fw - R,         HnewTemp1.w - Q);
                    HnewTemp2.w = max(HnewTemp2.w, Fw);
                    HnewTemp2.w = max(HnewTemp2.w, 0 );
                    Smax = max(HnewTemp2.w, Smax);

                    //store the values
                    Enew[j * BLOCKSIZE + threadIdx.x] = EnewTemp;
                    Hnew[(j-1) * BLOCKSIZE + threadIdx.x] = HnewTemp1;
                }
                Hnew[QCACHEBLOCK * BLOCKSIZE + threadIdx.x] = HnewTemp2;

                //update diagonal value
                HoldTemp2w = diag;
                //store horizontal H and F (only 4 values)
                Hrow[warpDBOffset + (i - offsetStart)/DBCACHEBLOCK * WARPSIZE + threadInWarpId] = HnewTemp2;
                Frow[warpDBOffset + (i - offsetStart)/DBCACHEBLOCK * WARPSIZE + threadInWarpId] = make_char4(Fx, Fy, Fz, Fw);

                //swap old and new scoring matrix columns
                temp = Hold;
                Hold = Hnew;
                Hnew = temp;
                temp = Eold;
                Eold = Enew;
                Enew = temp;
            }

            //initialize Hold, Eold to 0s
            for(size_t j = 1; j < QCACHEBLOCK + 1; j++){
                Eold[j * BLOCKSIZE + threadIdx.x].w = 0;
                Hold[j * BLOCKSIZE + threadIdx.x].w = 0;
            }
        }

        //update the scores
        scores[threadId] = Smax;
    }
};

int main(int argc, char* argv[]){
    // Extract the file names from the command line arguments
    std::string queryFileName, databaseFileName;
    size_t topScoresToPrint = 20; //default print the top 20
    int gapo = 11; //gap open penalty default value
    int gape = 1; //gap extension penalty default value

    // Minimum required arguments
    if (argc < 3) {
        std::cerr << "Program usage: './gaaswa query.fa database.fa [-top topScoresToPrint] [-gapo value] [-gape value]'" << std::endl;
        return 1;
    }

    queryFileName = argv[1];
    databaseFileName = argv[2];

    // Parse optional arguments
    for (int i = 3; i < argc; i++) {
        std::string arg = argv[i];
        if (arg == "-top") {
            if (i + 1 == argc) { // No more arguments after this flag
                std::cerr << "Error: -top requires a number." << std::endl;
                return 1;
            }
            topScoresToPrint = std::stoi(argv[++i]); // Increment 'i' to skip next argument.
        } else if (arg == "-gapo") {
            if (i + 1 == argc) {
                std::cerr << "Error: -gapo requires a number." << std::endl;
                return 1;
            }
            gapo = std::stoi(argv[++i]);
        } else if (arg == "-gape") {
            if (i + 1 == argc) {
                std::cerr << "Error: -gape requires a number." << std::endl;
                return 1;
            }
            gape = std::stoi(argv[++i]);
        } else {
            std::cerr << "Error: Unknown flag " << arg << std::endl;
            return 1;
        }
    }

    //Q and R gap values for the alignment algorithm
    int Q = gapo + gape;
    int R = gape;

    //read the fasta and convert to integers with offsets
    std::tuple<std::vector<scoreInt>, std::vector<size_t>, std::vector<size_t>, size_t, std::vector<size_t>, std::vector<std::string>> queryTuple = fastaReader(queryFileName, QCACHEBLOCK);
    std::tuple<std::vector<scoreInt>, std::vector<size_t>, std::vector<size_t>, size_t, std::vector<size_t>, std::vector<std::string>> databaseTuple = fastaReader(databaseFileName, DBCACHEBLOCK);
    std::vector<scoreInt> queryIntSequence = std::get<0>(queryTuple);
    std::vector<size_t> queryStartOffsets = std::get<1>(queryTuple);
    std::vector<size_t> queryEndOffsets = std::get<2>(queryTuple);
    size_t queryNumProt = std::get<3>(queryTuple); //total number of query proteins
    std::vector<size_t> queryLetterCounts = std::get<4>(queryTuple); //without the padding
    std::vector<std::string> queryProteinNames = std::get<5>(queryTuple);

    std::vector<scoreInt> databaseIntSequence = std::get<0>(databaseTuple);
    std::vector<size_t> databaseStartOffsets = std::get<1>(databaseTuple);
    std::vector<size_t> databaseEndOffsets = std::get<2>(databaseTuple);
    size_t databaseSize = std::get<3>(databaseTuple);
    std::vector<size_t> databaseLetterCounts = std::get<4>(databaseTuple); //without the padding
    std::vector<std::string> dbProteinNames = std::get<5>(databaseTuple);
    size_t databaseLetterCount = std::accumulate(databaseLetterCounts.begin(), databaseLetterCounts.end(), static_cast<size_t>(0)); //total letters without padding

    std::cout << "Number of proteins in the database: " << databaseSize << std::endl;
    std::cout << "Number of amino acids in the database: " << databaseLetterCount << std::endl;
    std::cout << "Number of amino acids in the database padded: " << databaseIntSequence.size() << std::endl;

    //MultiGPU
    int gpuCount;
    cudaGetDeviceCount(&gpuCount);
    std::cout << "Number of GPUs: " << gpuCount << std::endl;

    size_t gpuDBstart[gpuCount]; //starting protein for each GPU
    size_t gpuDBend[gpuCount]; //ending protein for each GPU
    size_t gpuDBcount[gpuCount]; //total protein count for each GPU

    //number of letters per protein in db from offsets (with padding) for sorting
    std::vector<size_t> databaseProteinSizes[gpuCount];

    //total number of amino acids per gpu with padding
    size_t databaseIntSequenceSize[gpuCount];

    //Index vectors for sorting
    std::vector<size_t> idx[gpuCount];

    //Offsets and names sorted by protein length
    std::vector<size_t> databaseStartOffsets_sorted[gpuCount];
    std::vector<size_t> databaseEndOffsets_sorted[gpuCount];
    std::vector<std::string> dbProteinNames_sorted[gpuCount];

    //Later used for allocating least amount of necessary memory for Hrow, Frow
    size_t totalWarps[gpuCount];
    std::vector<size_t> warpMaxProteinOffsets[gpuCount];
    std::vector<std::string> dbProteinNamesInt8;

    for( int gpuId = 0; gpuId < gpuCount; gpuId++ ){
        //figure out protein count for each GPU
        gpuDBstart[gpuId] = databaseSize * gpuId / gpuCount; //the order of * and / is important here when not divisible
        gpuDBend[gpuId] = databaseSize * (gpuId + 1) / gpuCount;
        gpuDBcount[gpuId] = gpuDBend[gpuId] - gpuDBstart[gpuId];

        //figure out how long those proteins are
        for(size_t i = 0; i < gpuDBcount[gpuId]; i++){
            databaseProteinSizes[gpuId].push_back(databaseEndOffsets[gpuDBstart[gpuId] + i] - databaseStartOffsets[gpuDBstart[gpuId] + i]);
        }

        //figure out the total number of amino acids per GPU (with padding)
        databaseIntSequenceSize[gpuId] = std::accumulate(databaseProteinSizes[gpuId].begin(), databaseProteinSizes[gpuId].end(), static_cast<size_t>(0));

        //fill index vector
        idx[gpuId].resize(gpuDBcount[gpuId]);
        std::iota(idx[gpuId].begin(), idx[gpuId].end(), 0);

        //Sort indexes based on comparing values in databaseProteinSizes
        auto temp = databaseProteinSizes[gpuId];
        std::sort(idx[gpuId].begin(), idx[gpuId].end(), [&temp](size_t i1, size_t i2) {return temp[i1] > temp[i2];});

        //Rearrange the offsets and names
        databaseStartOffsets_sorted[gpuId].resize(gpuDBcount[gpuId]);
        databaseEndOffsets_sorted[gpuId].resize(gpuDBcount[gpuId]);
        dbProteinNames_sorted[gpuId].resize(gpuDBcount[gpuId]);
        for (size_t i = 0; i < gpuDBcount[gpuId]; i++) {
            databaseStartOffsets_sorted[gpuId][i] = databaseStartOffsets[gpuDBstart[gpuId] + idx[gpuId][i]] - databaseStartOffsets[gpuDBstart[gpuId]]; //subtraction makes sure it starts from 0 on each GPU
            databaseEndOffsets_sorted[gpuId][i] = databaseEndOffsets[gpuDBstart[gpuId] + idx[gpuId][i]] - databaseStartOffsets[gpuDBstart[gpuId]];
            dbProteinNames_sorted[gpuId][i] = dbProteinNames[gpuDBstart[gpuId] + idx[gpuId][i]];
        }
        dbProteinNamesInt8.insert(dbProteinNamesInt8.end(), dbProteinNames_sorted[gpuId].begin(), dbProteinNames_sorted[gpuId].end()); //collecting the sorted names for later printing usage

        //used to allocate the least amount of necessary memory for Hrow, Frow
        totalWarps[gpuId] = (gpuDBcount[gpuId] + WARPSIZE - 1)/WARPSIZE;
        warpMaxProteinOffsets[gpuId].resize(totalWarps[gpuId] + 1);
        warpMaxProteinOffsets[gpuId][0] = 0;
        for (size_t i = 1; i < totalWarps[gpuId] + 1; i++){
            warpMaxProteinOffsets[gpuId][i] = warpMaxProteinOffsets[gpuId][i-1] + WARPSIZE * databaseProteinSizes[gpuId][idx[gpuId][ WARPSIZE * ( i - 1 ) ]] / DBCACHEBLOCK;
        }
    }


    //Declare c-style arrays to be filled by the vectors later
    scoreInt *qArray_h;
    scoreInt *dbArray_h[gpuCount];
    scoreInt *dbArray_d[gpuCount];
    size_t *dbStartOffsets_h[gpuCount];
    size_t *dbStartOffsets_d[gpuCount];
    size_t *dbEndOffsets_h[gpuCount];
    size_t *dbEndOffsets_d[gpuCount];
    size_t *warpDBOffsets_h[gpuCount];
    size_t *warpDBOffsets_d[gpuCount];
    scoreInt *blosum_h;
    scoreInt *blosum_d[gpuCount];
    scoreInt *scores_h;
    scoreInt *scores_d[gpuCount];
    //scoring matrices storing rows for enabling cache blocking
    packed4Int* Hrow[gpuCount];
    packed4Int* Frow[gpuCount];

    //allocate memory on CPU
    for( int gpuId = 0; gpuId < gpuCount; gpuId++ ){
        cudaMallocHost(&dbArray_h[gpuId], databaseIntSequenceSize[gpuId] * sizeof(scoreInt));
        cudaMallocHost(&dbStartOffsets_h[gpuId], databaseStartOffsets_sorted[gpuId].size() * sizeof(size_t));
        cudaMallocHost(&dbEndOffsets_h[gpuId], databaseEndOffsets_sorted[gpuId].size() * sizeof(size_t));
        cudaMallocHost(&warpDBOffsets_h[gpuId], (totalWarps[gpuId] + 1) * sizeof(size_t));
    }
    cudaMallocHost(&scores_h, databaseSize * sizeof(scoreInt));
    cudaMallocHost(&blosum_h, 25 * 25 * sizeof(scoreInt)); //later put on shared memory in the kernel

    /* Fill on the CPU side */
    //blosum matrix
    for(size_t i = 0; i < (25*25); i++){
        blosum_h[i] = s_blosum62[i];
    }

    //copying vectors to c-style arrays
    for( int gpuId = 0; gpuId < gpuCount; gpuId++ ){
        std::copy(databaseIntSequence.begin() + databaseStartOffsets[gpuDBstart[gpuId]], databaseIntSequence.begin() + databaseEndOffsets[gpuDBend[gpuId] - 1], dbArray_h[gpuId]);
        std::copy(databaseStartOffsets_sorted[gpuId].begin(), databaseStartOffsets_sorted[gpuId].end(), dbStartOffsets_h[gpuId]);
        std::copy(databaseEndOffsets_sorted[gpuId].begin(), databaseEndOffsets_sorted[gpuId].end(), dbEndOffsets_h[gpuId]);
        std::copy(warpMaxProteinOffsets[gpuId].begin(), warpMaxProteinOffsets[gpuId].end(), warpDBOffsets_h[gpuId]);
    }

    /* Send data to GPU */
    for( int gpuId = 0; gpuId < gpuCount; gpuId++ ){
        //select the gpu
        cudaSetDevice(gpuId);

        //allocate memory on GPU (blosum, db, offsets, scores)
        cudaMalloc(&blosum_d[gpuId], 25 * 25 * sizeof(scoreInt));
        cudaMalloc(&dbArray_d[gpuId], databaseIntSequenceSize[gpuId] * sizeof(scoreInt));
        cudaMalloc(&dbStartOffsets_d[gpuId], databaseStartOffsets_sorted[gpuId].size() * sizeof(size_t));
        cudaMalloc(&dbEndOffsets_d[gpuId], databaseEndOffsets_sorted[gpuId].size() * sizeof(size_t));
        cudaMalloc(&warpDBOffsets_d[gpuId], (totalWarps[gpuId] + 1) * sizeof(size_t));
        cudaMalloc(&scores_d[gpuId], gpuDBcount[gpuId] * sizeof(scoreInt));
        cudaMalloc(&Hrow[gpuId], warpMaxProteinOffsets[gpuId][totalWarps[gpuId]] * sizeof(packed4Int));
        cudaMalloc(&Frow[gpuId], warpMaxProteinOffsets[gpuId][totalWarps[gpuId]] * sizeof(packed4Int));

        //send to GPU (blosum, db, offsets, scores)
        cudaMemcpyAsync(blosum_d[gpuId], blosum_h, 25 * 25 * sizeof(scoreInt), cudaMemcpyHostToDevice);
        cudaMemcpyAsync(dbArray_d[gpuId], dbArray_h[gpuId], databaseIntSequenceSize[gpuId] * sizeof(scoreInt), cudaMemcpyHostToDevice);
        cudaMemcpyAsync(dbStartOffsets_d[gpuId], dbStartOffsets_h[gpuId], databaseStartOffsets_sorted[gpuId].size() * sizeof(size_t), cudaMemcpyHostToDevice);
        cudaMemcpyAsync(dbEndOffsets_d[gpuId], dbEndOffsets_h[gpuId], databaseEndOffsets_sorted[gpuId].size() * sizeof(size_t), cudaMemcpyHostToDevice);
        cudaMemcpyAsync(warpDBOffsets_d[gpuId], warpDBOffsets_h[gpuId], (totalWarps[gpuId] + 1) * sizeof(size_t), cudaMemcpyHostToDevice);
    }

    for(size_t i = 0; i < queryNumProt; i++){
        //figure out query length from the start and end offsets
        size_t qLength = queryEndOffsets[i] - queryStartOffsets[i]; //with padding
        size_t qLengthNoPad = queryLetterCounts[i]; //same as qLength but without padding counted
        std::cout << "----------------------------------------" << std::endl;
        std::cout << "Query: " << queryProteinNames[i] << std::endl;
        std::cout << "Query length: " << qLengthNoPad << std::endl;

        /* Time the int8 + int32 kernel */
        auto timeStartInt8 = std::chrono::high_resolution_clock::now();

        cudaMallocHost(&qArray_h, qLength * sizeof(scoreInt));
        std::copy(&queryIntSequence[queryStartOffsets[i]], &queryIntSequence[queryEndOffsets[i]], qArray_h);

        for( int gpuId = 0; gpuId < gpuCount; gpuId++ ){
            cudaSetDevice(gpuId);

            //printMemoryUsage();
            //initialize the scores to 0s
            cudaMemset(scores_d[gpuId], 0, gpuDBcount[gpuId] * sizeof(scoreInt));
            //send to GPU constant memory(query)
            cudaMemcpyToSymbol(qArray_constant, qArray_h, qLength * sizeof(scoreInt));
            //initialize those to 0s
            cudaMemset(Hrow[gpuId], 0, warpMaxProteinOffsets[gpuId][totalWarps[gpuId]] * sizeof(packed4Int));
            cudaMemset(Frow[gpuId], 0, warpMaxProteinOffsets[gpuId][totalWarps[gpuId]] * sizeof(packed4Int));
        }

        /* Align the sequences on GPU with int8 kernel*/
        for( int gpuId = 0; gpuId < gpuCount; gpuId++ ){
            cudaSetDevice(gpuId);

            size_t blockSize = BLOCKSIZE;
            size_t nBlocks = (gpuDBcount[gpuId] + blockSize - 1)/blockSize;
            dim3 dimGrid(nBlocks, 1, 1);
            dim3 dimBlock(blockSize, 1, 1);

            //int8 kernel
            alignSingleQuery<<<dimGrid, dimBlock>>>(qLength, dbArray_d[gpuId], gpuDBcount[gpuId], dbStartOffsets_d[gpuId], dbEndOffsets_d[gpuId], warpDBOffsets_d[gpuId], blosum_d[gpuId], scores_d[gpuId], Hrow[gpuId], Frow[gpuId], Q, R);

            //Recieve the results from GPU
            cudaMemcpyAsync(&scores_h[gpuDBstart[gpuId]], scores_d[gpuId], gpuDBcount[gpuId] * sizeof(scoreInt), cudaMemcpyDeviceToHost);
        }

        for( int gpuId = 0; gpuId < gpuCount; gpuId++ ){
            cudaSetDevice(gpuId);
            cudaDeviceSynchronize();
        }

        auto timeEndInt8 = std::chrono::high_resolution_clock::now();
        auto timeTotalInt8 = std::chrono::duration_cast<std::chrono::microseconds>(timeEndInt8 - timeStartInt8).count() / 1000.0;
        std::cout << "Alignment time (int8 only): "<< timeTotalInt8 << " ms" << std::endl;

        auto timeStartCollectOverflow = std::chrono::high_resolution_clock::now();
        /* Deal with the integer overflow */
        std::vector<size_t> databaseStartOffsetsOverflown[gpuCount];
        std::vector<size_t> databaseEndOffsetsOverflown[gpuCount];
        std::vector<size_t> dbProteinIndicesOverflown[gpuCount];
        size_t overflowNum[gpuCount];

        for( int gpuId = 0; gpuId < gpuCount; gpuId++ ){
            overflowNum[gpuId] = 0;
            for (size_t i = 0; i < gpuDBcount[gpuId]; i++) {
                if( scores_h[gpuDBstart[gpuId] + i] > INTOVERFLOW ){
                    overflowNum[gpuId]++;
                    databaseStartOffsetsOverflown[gpuId].push_back(dbStartOffsets_h[gpuId][i]);
                    databaseEndOffsetsOverflown[gpuId].push_back(dbEndOffsets_h[gpuId][i]);
                    dbProteinIndicesOverflown[gpuId].push_back(i); //later used to print out the names
                }
            }
        }
        auto timeEndCollectOverflow = std::chrono::high_resolution_clock::now();
        auto timeTotalCollectOverflow = std::chrono::duration_cast<std::chrono::microseconds>(timeEndCollectOverflow - timeStartCollectOverflow).count() / 1000.0;
        std::cout << "Collect overflow time: "<< timeTotalCollectOverflow << " ms" << std::endl;

        auto timeStartIntLonger = std::chrono::high_resolution_clock::now();
        /* Allocate memory for scoresNew, dbOffsetsNew, Hold Hnew Eold Enew */
        //Declare c-style arrays to be filled by the vectors later
        size_t *dbStartOffsetsNew_h[gpuCount];
        size_t *dbStartOffsetsNew_d[gpuCount];
        size_t *dbEndOffsetsNew_h[gpuCount];
        size_t *dbEndOffsetsNew_d[gpuCount];
        scoreIntLonger *scoresNew_h[gpuCount];
        scoreIntLonger *scoresNew_d[gpuCount];

        for( int gpuId = 0; gpuId < gpuCount; gpuId++ ){
            //allocate memory on CPU
            cudaMallocHost(&dbStartOffsetsNew_h[gpuId], overflowNum[gpuId] * sizeof(size_t));
            cudaMallocHost(&dbEndOffsetsNew_h[gpuId], overflowNum[gpuId] * sizeof(size_t));
            cudaMallocHost(&scoresNew_h[gpuId], overflowNum[gpuId] * sizeof(scoreIntLonger));

            //copying vectors to c-style arrays
            std::copy(databaseStartOffsetsOverflown[gpuId].begin(), databaseStartOffsetsOverflown[gpuId].end(), dbStartOffsetsNew_h[gpuId]);
            std::copy(databaseEndOffsetsOverflown[gpuId].begin(), databaseEndOffsetsOverflown[gpuId].end(), dbEndOffsetsNew_h[gpuId]);
        }

        /* Send data to GPU */
        //allocate scoring matrices storing columns (GPU only)
        packed4IntLonger* Hold[gpuCount];
        packed4IntLonger* Hnew[gpuCount];
        packed4IntLonger* Eold[gpuCount];
        packed4IntLonger* Enew[gpuCount];
        for( int gpuId = 0; gpuId < gpuCount; gpuId++ ){
            cudaSetDevice(gpuId);

            //allocate memory on GPU (offsets, scores)
            cudaMalloc(&dbStartOffsetsNew_d[gpuId], overflowNum[gpuId] * sizeof(size_t));
            cudaMalloc(&dbEndOffsetsNew_d[gpuId], overflowNum[gpuId] * sizeof(size_t));
            cudaMalloc(&scoresNew_d[gpuId], overflowNum[gpuId] * sizeof(scoreIntLonger));

            //send to GPU (offsets)
            cudaMemcpyAsync(dbStartOffsetsNew_d[gpuId], dbStartOffsetsNew_h[gpuId], overflowNum[gpuId] * sizeof(size_t), cudaMemcpyHostToDevice);
            cudaMemcpyAsync(dbEndOffsetsNew_d[gpuId], dbEndOffsetsNew_h[gpuId], overflowNum[gpuId] * sizeof(size_t), cudaMemcpyHostToDevice);

            cudaMalloc(&Hold[gpuId], overflowNum[gpuId] * (qLength + 1) * sizeof(packed4IntLonger));
            cudaMalloc(&Eold[gpuId], overflowNum[gpuId] * (qLength + 1) * sizeof(packed4IntLonger));
            cudaMalloc(&Hnew[gpuId], overflowNum[gpuId] * (qLength + 1) * sizeof(packed4IntLonger));
            cudaMalloc(&Enew[gpuId], overflowNum[gpuId] * (qLength + 1) * sizeof(packed4IntLonger));

            //initialize those to 0s
            cudaMemset(Hold[gpuId], 0, overflowNum[gpuId] * (qLength + 1) * sizeof(packed4IntLonger));
            cudaMemset(Eold[gpuId], 0, overflowNum[gpuId] * (qLength + 1) * sizeof(packed4IntLonger));
        }

        /* Align the overflown ones with int16 kernel */
        for( int gpuId = 0; gpuId < gpuCount; gpuId++ ){
            cudaSetDevice(gpuId);

            size_t blockSize = BLOCKSIZE;
            size_t nBlocks = (overflowNum[gpuId] + blockSize - 1)/blockSize;
            dim3 dimGrid2(nBlocks, 1, 1);
            dim3 dimBlock2(blockSize, 1, 1);

            alignSingleQueryOverflown<<<dimGrid2, dimBlock2>>>(qLength, dbArray_d[gpuId], overflowNum[gpuId], dbStartOffsetsNew_d[gpuId], dbEndOffsetsNew_d[gpuId], Hold[gpuId], Hnew[gpuId], Eold[gpuId], Enew[gpuId], blosum_d[gpuId], scoresNew_d[gpuId], Q, R);
            //receive results from GPU
            cudaMemcpyAsync(scoresNew_h[gpuId], scoresNew_d[gpuId], overflowNum[gpuId] * sizeof(scoreIntLonger), cudaMemcpyDeviceToHost);
        }

        for( int gpuId = 0; gpuId < gpuCount; gpuId++ ){
            cudaSetDevice(gpuId);
            cudaDeviceSynchronize();
        }

        //Stop timer
        auto timeEndIntLonger = std::chrono::high_resolution_clock::now();
        auto timeTotalIntLonger = std::chrono::duration_cast<std::chrono::microseconds>(timeEndIntLonger - timeStartIntLonger).count() / 1000.0;
        double GCUPS = qLengthNoPad * (databaseLetterCount / (timeTotalInt8 + timeTotalCollectOverflow + timeTotalIntLonger) / 1E6);
        std::cout << "Re-Alignment time (overflown only): "<< timeTotalIntLonger << " ms" << std::endl;
        std::cout << "Performance (int8 + overflow re-alignment kernels): "<< GCUPS << " GCUPS" << std::endl;
        std::cout << "Alignment time: "<< timeTotalInt8 + timeTotalCollectOverflow + timeTotalIntLonger << " ms" << std::endl;

        /* Collect the overflow re-alignment score results */
        auto timeStart = std::chrono::high_resolution_clock::now();
        size_t totalOverflowNum = 0;
        std::vector<scoreIntLonger> scoresIntLonger;
        std::vector<std::string> dbProteinNamesIntLonger;
        for( int gpuId = 0; gpuId < gpuCount; gpuId++ ){
            
            totalOverflowNum += overflowNum[gpuId];
            scoresIntLonger.insert(scoresIntLonger.end(), scoresNew_h[gpuId], scoresNew_h[gpuId] + overflowNum[gpuId]);
            for ( size_t i = 0; i < overflowNum[gpuId]; i++ ){
                dbProteinNamesIntLonger.push_back(dbProteinNames_sorted[gpuId][dbProteinIndicesOverflown[gpuId][i]]);
            }
        }   
        auto timeEnd = std::chrono::high_resolution_clock::now();
        auto timeTotal = std::chrono::duration_cast<std::chrono::microseconds>(timeEnd - timeStart).count() / 1000.0;
        std::cout << "Collecting re-alignment scores and names into single vector time: "<< timeTotal << " ms" << std::endl;

        timeStart = std::chrono::high_resolution_clock::now();
        /* Sorting the overflown score results */
        // Refill index vector
        std::vector<size_t> idx2(totalOverflowNum);
        std::iota(idx2.begin(), idx2.end(), 0);

        // Sort indexes based on comparing scores
        std::sort(idx2.begin(), idx2.end(), [&scoresIntLonger](size_t i1, size_t i2) {return scoresIntLonger[i1] > scoresIntLonger[i2];});

        // Use sorted indexes to re-arrange all vectors
        std::vector<scoreIntLonger> scoresIntLonger_sorted;
        std::vector<std::string> dbProteinNamesIntLonger_sorted;
        for (size_t i = 0; i < idx2.size(); i++) {
            scoresIntLonger_sorted.push_back(scoresIntLonger[idx2[i]]);
            dbProteinNamesIntLonger_sorted.push_back(dbProteinNamesIntLonger[idx2[i]]);
        }
        timeEnd = std::chrono::high_resolution_clock::now();
        timeTotal = std::chrono::duration_cast<std::chrono::microseconds>(timeEnd - timeStart).count() / 1000.0;
        std::cout << "Sorting overflown results time: "<< timeTotal << " ms" << std::endl;

        //Print out the best matches
        std::cout << "---------------------------" << std::endl;
        std::cout << "Best Matches" << std::endl;
        for (size_t i = 0; i < totalOverflowNum && i < topScoresToPrint; i++) {
            std::cout << scoresIntLonger_sorted[i] << "\t" << dbProteinNamesIntLonger_sorted[i] << std::endl;
        }
        timeStart = std::chrono::high_resolution_clock::now();
        if(totalOverflowNum < topScoresToPrint){
            /* Efficiently collects the top int8 scores and prints without sorting*/
            auto compare = [](const std::pair<scoreInt, int>& a, const std::pair<scoreInt, int>& b) {
                return a.first > b.first;
            };
            // Min-heap to maintain top values
            std::priority_queue<std::pair<scoreInt, int>, std::vector<std::pair<scoreInt, int>>, decltype(compare)> minHeap(compare);

            for (size_t i = 0; i < databaseSize; i++) {
                if (minHeap.size() < topScoresToPrint) {
                    minHeap.push({scores_h[i], i});
                } else if (scores_h[i] > minHeap.top().first) {
                    minHeap.pop();
                    minHeap.push({scores_h[i], i});
                }
            }

            std::vector<std::pair<scoreInt, int>> bestScoresInt8;
            while (!minHeap.empty()) {
                bestScoresInt8.push_back(minHeap.top());
                minHeap.pop();
            }

            for (size_t i = bestScoresInt8.size() - totalOverflowNum; i > 0; i--) {
                std::cout << (int)bestScoresInt8[i-1].first << "\t" << dbProteinNamesInt8[bestScoresInt8[i-1].second] << std::endl;
            }
        }
        timeEnd = std::chrono::high_resolution_clock::now();
        timeTotal = std::chrono::duration_cast<std::chrono::microseconds>(timeEnd - timeStart).count() / 1000.0;
        std::cout << "Collecting top int8 scores time: "<< timeTotal << " ms" << std::endl;

        timeStart = std::chrono::high_resolution_clock::now();
        /* Cleanup */
        for( int gpuId = 0; gpuId < gpuCount; gpuId++ ){
            cudaSetDevice(gpuId);

            cudaFree(dbStartOffsetsNew_d[gpuId]);
            cudaFree(dbEndOffsetsNew_d[gpuId]);
            cudaFreeHost(scoresNew_h[gpuId]);
            cudaFree(scoresNew_d[gpuId]);

            cudaFree(Hold[gpuId]);
            cudaFree(Hnew[gpuId]);
            cudaFree(Eold[gpuId]);
            cudaFree(Enew[gpuId]);

            cudaFreeHost(dbStartOffsetsNew_h[gpuId]);
            cudaFreeHost(dbEndOffsetsNew_h[gpuId]);
        }
        cudaFreeHost(qArray_h);
        timeEnd = std::chrono::high_resolution_clock::now();
        timeTotal = std::chrono::duration_cast<std::chrono::microseconds>(timeEnd - timeStart).count() / 1000.0;
        std::cout << "Cleanup time: "<< timeTotal << " ms" << std::endl;
    }

    /* Cleanup */
    for( int gpuId = 0; gpuId < gpuCount; gpuId++ ){
        cudaSetDevice(gpuId);

        cudaFree(dbArray_d[gpuId]);
        cudaFree(dbStartOffsets_d[gpuId]);
        cudaFree(dbEndOffsets_d[gpuId]);
        cudaFree(warpDBOffsets_d[gpuId]);
        cudaFree(blosum_d[gpuId]);
        cudaFree(scores_d[gpuId]);
        cudaFree(Hrow[gpuId]);
        cudaFree(Frow[gpuId]);

        cudaFreeHost(dbArray_h[gpuId]);
        cudaFreeHost(dbStartOffsets_h[gpuId]);
        cudaFreeHost(dbEndOffsets_h[gpuId]);
        cudaFreeHost(warpDBOffsets_h[gpuId]);
    }
    cudaFreeHost(scores_h);
    cudaFreeHost(blosum_h);

    return 0;
}